/*
 Navicat Premium Data Transfer

 Source Server         : springboot
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : springboot1

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 09/03/2023 10:14:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for carousel
-- ----------------------------
DROP TABLE IF EXISTS `carousel`;
CREATE TABLE `carousel`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `carousel` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of carousel
-- ----------------------------
INSERT INTO `carousel` VALUES (1, '张三', '/image/carousel/1.png');
INSERT INTO `carousel` VALUES (2, '李四', '/image/carousel/2.png');
INSERT INTO `carousel` VALUES (3, '王五', '/image/carousel/3.png');
INSERT INTO `carousel` VALUES (4, '王五', '/image/carousel/4.png');

-- ----------------------------
-- Table structure for jdbc_test
-- ----------------------------
DROP TABLE IF EXISTS `jdbc_test`;
CREATE TABLE `jdbc_test`  (
  `ds_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `ds_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源类型',
  `ds_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据源名称',
  PRIMARY KEY (`ds_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jdbc_test
-- ----------------------------
INSERT INTO `jdbc_test` VALUES (1, 'com.zaxxer.hikari.HikariDataSource', 'hikari数据源');
INSERT INTO `jdbc_test` VALUES (2, 'y', 'y');

-- ----------------------------
-- Table structure for post
-- ----------------------------
DROP TABLE IF EXISTS `post`;
CREATE TABLE `post`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '帖子id',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `subTitle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of post
-- ----------------------------
INSERT INTO `post` VALUES (1, '/image/post/1.png', '1', '1');
INSERT INTO `post` VALUES (2, '/image/post/2.png', '2', '2');
INSERT INTO `post` VALUES (3, '/image/post/3.png', '3', '3');
INSERT INTO `post` VALUES (4, '/image/post/4.png', '4', '4');
INSERT INTO `post` VALUES (5, '/image/post/5.png', '5', '5');
INSERT INTO `post` VALUES (6, '/image/post/6.png', '6', '6');
INSERT INTO `post` VALUES (7, '/image/post/7.png', '7', '7');
INSERT INTO `post` VALUES (8, '/image/post/8.png', '8', '8');
INSERT INTO `post` VALUES (9, '/image/post/9.png', '9', '9');
INSERT INTO `post` VALUES (10, '/image/post/10.png', '10', '10');
INSERT INTO `post` VALUES (11, '/image/post/11.png', '11', '11');
INSERT INTO `post` VALUES (12, '/image/post/12.png', '12', '12');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录名',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 'Spring Boot', '123456');
INSERT INTO `tb_user` VALUES (2, 'MyBatis', '123456');
INSERT INTO `tb_user` VALUES (3, 'Thymeleaf', '123456');
INSERT INTO `tb_user` VALUES (4, 'Java', '123456');
INSERT INTO `tb_user` VALUES (5, 'MySQL', '123456');
INSERT INTO `tb_user` VALUES (6, 'IDEA', '123456');

-- ----------------------------
-- Table structure for water
-- ----------------------------
DROP TABLE IF EXISTS `water`;
CREATE TABLE `water`  (
  `id` int(0) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `salary` double(10, 2) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of water
-- ----------------------------
INSERT INTO `water` VALUES (1, '张三', 10000.00);
INSERT INTO `water` VALUES (2, '王麻子', 8000.00);
INSERT INTO `water` VALUES (4, '小丸子', 12000.00);

SET FOREIGN_KEY_CHECKS = 1;
