import 'package:flutter/material.dart';

main(List<String> args) {
  return runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('标题'),
        ),
        body: MyContentWidget(),
      ),
    );
  }
}

class MyContentWidget extends StatefulWidget {
  MyContentWidget({Key? key}) : super(key: key);

  @override
  _MyContentWidgetState createState() => _MyContentWidgetState();
}

class _MyContentWidgetState extends State<MyContentWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: MyContainer(
                  color: Colors.blue,
                ),
                flex: 1,
              ),
              Padding(
                padding: EdgeInsets.only(
                  left: 10.0,
                ),
              ),
              Expanded(
                child: MyContainer(color: Colors.green),
                flex: 2,
              )
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Row(
            children: <Widget>[
              Expanded(child: MyContainer(color: Colors.pink), flex: 2),
              Padding(
                padding: EdgeInsets.only(
                  left: 10.0,
                ),
              ),
              Expanded(
                child: Container(
                  height: 80.0,
                  child: Column(
                    children: <Widget>[
                      Expanded(
                          child: Container(
                            color: Colors.red,
                            child: Center(
                              child: Icon(Icons.access_time),
                            ),
                          ),
                          flex: 1),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      Expanded(
                          child: Container(
                            color: Colors.red,
                            child: Center(
                              child: Icon(Icons.access_time),
                            ),
                          ),
                          flex: 1),
                    ],
                  ),
                ),
                flex: 1,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class MyContainer extends StatelessWidget {
  final Color color;
  final double height;
  const MyContainer({Key? key, required this.color, this.height = 80.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      color: color,
      child: Center(
        child: Icon(
          Icons.ac_unit,
          color: Color(0xffffffff),
        ),
      ),
    );
  }
}
