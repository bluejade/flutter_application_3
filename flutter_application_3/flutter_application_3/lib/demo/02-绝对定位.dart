import 'package:flutter/material.dart';

main(List<String> args) {
  return runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('标题'),
        ),
        body: MyContentWidget(),
      ),
    );
  }
}

class MyContentWidget extends StatefulWidget {
  MyContentWidget({Key? key}) : super(key: key);

  @override
  _MyContentWidgetState createState() => _MyContentWidgetState();
}

class _MyContentWidgetState extends State<MyContentWidget> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Positioned(child: Text('123'), bottom: 20, right: 20),
        Positioned(child: Text('123'), bottom: 20, left: 20),
      ],
    );
  }
}
