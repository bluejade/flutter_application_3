import 'package:flutter/material.dart';

class EditPage extends StatefulWidget {
  String str;
  String title;
  EditPage({Key? key, this.str = '', this.title = ''}) : super(key: key);

  @override
  _EditPageState createState() => _EditPageState(this.str);
}

class _EditPageState extends State<EditPage> {
  String str;
  _EditPageState(this.str);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('编辑页面标题')),
      body: Text('编辑页面内容----$str'),
    );
  }
}
