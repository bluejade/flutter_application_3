// ignore_for_file: prefer_const_constructors, sort_child_properties_last, import_of_legacy_library_into_null_safe, unused_local_variable, unnecessary_new, avoid_print

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import './component/swiper.dart';
import './component/ItemList.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({super.key});

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  List carousels = [];
  List banners = [];
  List feeds = [];
  String key = "0";
  int pageNum = 1;
  int pageSize = 3;
  ScrollController scrollcontroller = new ScrollController();

  @override
  void initState() {
    // 到达底部，再次调用数据，即分页-下一页
    scrollcontroller.addListener(() {
      if (scrollcontroller.position.pixels ==
          scrollcontroller.position.maxScrollExtent) {
        // getData();
        print(pageNum);
        print(pageNum);
        pageNum += 1;

        print(pageNum);
        print(pageNum);
        // getFeedbyGet();
        getFeedbyPost();
      }
    });

    getBanner();
    // getFeed();
    // getFeedbyGet();
    getFeedbyPost();
    // getData();
    super.initState();
  }

  // 轮播图
  void getBanner() async {
    Response res2 = await Dio().get('http://localhost:8081/carousel/selectAll');

    banners = res2.data["data"];
    // print("banners1234");
    // print(banners);
  }

  // 列表数据
  void getFeed() async {
    print('getFeed');
    Response res = await Dio().get('http://localhost:8081/post/selectAll');

    print('getFeed2');
    setState(() {
      feeds = res.data["data"];
    });
    print("feeds");
    print(feeds);
  }

  // 列表数据
  void getFeedbyGet() async {
    Response res = await Dio().get(
        'http://localhost:8081/post/findPagebyGet?pageNum=$pageNum&pageSize=$pageSize');

    // print('getFeedbyGet：');
    // print(res);
    setState(() {
      feeds = res.data["data"];
      // feeds.addAll(res.data["data"]);
    });
    print("feeds");
    print(feeds);
  }

  // 列表数据
  void getFeedbyPost() async {
    Response res = await Dio().post('http://localhost:8081/post/findPagebyPost',
        data: {'pageNum': pageNum, 'pageSize': pageSize});

    print('getFeedbyPost');
    print(res);
    setState(() {
      feeds = res.data["data"];
      // feeds.addAll(res.data["data"]);
    });
    print("feeds");
    print(feeds);
  }

  Future<void> onRefresh() async {
    // 上拉刷新数据
    // key = "0";
    // return getData();
    pageNum = 1;
    print(pageNum);
    // return getFeedbyGet();
    return getFeedbyPost();
  }

  void getData() async {
    Response res =
        await Dio().get('http://app3.qdaily.com/app3/homes/index_v2/$key.json');

    setState(() {
      if (key == '0') {
        // banners = res.data["response"]["banners"];
        feeds = res.data["response"]["feeds"];
      } else {
        feeds.addAll(res.data["response"]["feeds"]);
      }
    });
    // 每加载一次数据更新key，即分页参数
    key = res.data["response"]["last_key"];
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      child: ListView.separated(
        controller: scrollcontroller,
        itemBuilder: (context, index) {
          if (index == 0) {
            // 轮播图
            return MySwiper(banners);
          }
          // 列表数据
          return ItemList(feeds[index - 1]);
          // return Container(
          //     height: 100, child: Text(feeds[index]['post']['title']));
        },
        separatorBuilder: (BuildContext context, int index) {
          return Container(
            height: 5.0,
            color: Colors.redAccent,
          );
        },
        itemCount: feeds.length + 1,
      ),
      onRefresh: onRefresh,
    );
  }
}
