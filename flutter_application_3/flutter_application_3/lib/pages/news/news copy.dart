// ignore_for_file: prefer_const_constructors, sort_child_properties_last, import_of_legacy_library_into_null_safe, unused_local_variable, unnecessary_new, avoid_print

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import './component/swiper.dart';
import './component/ItemList.dart';

class NewsPage extends StatefulWidget {
  const NewsPage({super.key});

  @override
  State<NewsPage> createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  List carousels = [];
  List banners = [];
  List feeds = [];
  String key = "0";
  ScrollController scrollcontroller = new ScrollController();

  late List resCarousel;

  @override
  void initState() {
    // 到达底部，再次调用数据，即分页-下一页
    scrollcontroller.addListener(() {
      if (scrollcontroller.position.pixels ==
          scrollcontroller.position.maxScrollExtent) {
        print("1234");
        getData();
      }
    });

    getData();
    super.initState();
  }

  Future<void> onRefresh() async {
    // 上拉刷新数据
    print(key);
    key = "0";
    return getData();
  }

  void getData() async {
    print("key");
    print(key);

    Response res =
        await Dio().get('http://app3.qdaily.com/app3/homes/index_v2/$key.json');

    Response resCarousel =
        await Dio().get('http://localhost:8081/carousel/selectAll');

    // carousels = resCarousel;
    print("resCarousel");
    print(resCarousel);
    print(resCarousel);
    print("5678");

    setState(() {
      if (key == '0') {
        // banners = res.data["response"]["banners"];
        feeds = res.data["response"]["feeds"];
      } else {
        feeds.addAll(res.data["response"]["feeds"]);
      }
    });
    // 每加载一次数据更新key，即分页参数
    key = res.data["response"]["last_key"];
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      child: ListView.separated(
        controller: scrollcontroller,
        itemBuilder: (context, index) {
          if (index == 0) {
            // 轮播图
            // return MySwiper(banners);
            return MySwiper(resCarousel);
          }
          // 列表数据
          return ItemList(feeds[index - 1]);
          // return Container(
          //     height: 100, child: Text(feeds[index]['post']['title']));
        },
        separatorBuilder: (BuildContext context, int index) {
          return Container(
            height: 5.0,
            color: Colors.redAccent,
          );
        },
        itemCount: feeds.length + 1,
      ),
      onRefresh: onRefresh,
    );
  }
}
