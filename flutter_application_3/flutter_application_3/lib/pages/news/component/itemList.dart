import 'package:flutter/material.dart';

class ItemList extends StatelessWidget {
  Map feed;
  String baseUrl = 'http://localhost:8081';
  ItemList(this.feed, {super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            height: 200,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(baseUrl + feed["image"]),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Text(
            // feed['post']['title'],
            feed['title'],
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          // Text(feed['post']['description']),
          Text(feed['subTitle']),
        ],
      ),
    );
  }
}
