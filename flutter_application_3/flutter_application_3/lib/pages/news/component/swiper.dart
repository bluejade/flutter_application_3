// ignore_for_file: import_of_legacy_library_into_null_safe, unnecessary_new, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class MySwiper extends StatelessWidget {
  List banners;
  String baseUrl = 'http://localhost:8081';

  DotSwiperPaginationBuilder builder = new DotSwiperPaginationBuilder(
    activeColor: Color(0xffffc81f),
    space: 5.0,
    size: 7.0,
  );
  MySwiper(this.banners, {super.key});

  @override
  Widget build(BuildContext context) {
    // print("5678");
    // print(baseUrl);
    // print(banners);
    // print("5678");

    if (banners.length == 0) {
      return Container();
    }
    return Container(
      height: 200,
      child: Swiper(
        itemCount: banners.length,
        autoplay: true,
        pagination: SwiperPagination(builder: builder),
        itemBuilder: (context, index) {
          return Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                  baseUrl + banners[index]['carousel'],
                ),
                fit: BoxFit.cover,
              ),
            ),
          );
        },
      ),
    );
  }
}
