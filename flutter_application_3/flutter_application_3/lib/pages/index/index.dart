import 'package:flutter/material.dart';
import '/pages/home/home.dart';
import '/pages/edit/edit.dart';
import '/pages/me/me.dart';

class IndexPage extends StatefulWidget {
  String str;
  IndexPage({Key? key, this.str = ''}) : super(key: key);

  @override
  _IndexPageState createState() => _IndexPageState(this.str);
}

class _IndexPageState extends State<IndexPage> {
  int currentIdx = 0;
  // List test = [Text('1'), Text('2'), Text('3')];
  List test = [
    HomePage(),
    EditPage(
      title: '1',
    ),
    MePage()
  ];

  String str;
  _IndexPageState(this.str);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: Text('首页页面标题1')),
      body: Scaffold(
        // appBar: AppBar(
        //   title: Text('标题'),
        // ),
        body: test[currentIdx],
        bottomNavigationBar: BottomNavigationBar(
          onTap: (int index) {
            setState(() {
              currentIdx = index;
            });
          },
          currentIndex: currentIdx,
          selectedItemColor: Colors.red,
          unselectedItemColor: Colors.grey,
          // ignore: prefer_const_literals_to_create_immutables
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.ac_unit), label: 'A'),
            BottomNavigationBarItem(icon: Icon(Icons.ac_unit), label: 'B'),
            BottomNavigationBarItem(icon: Icon(Icons.ac_unit), label: 'C'),
          ],
        ),
      ),
    );
  }
}
