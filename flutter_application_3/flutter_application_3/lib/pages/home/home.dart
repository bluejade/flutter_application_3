import 'package:flutter/material.dart';
import 'package:flutter_application_3/pages/lab/lab.dart';
import 'package:flutter_application_3/pages/news/news.dart';
import '../search/search.dart';
import '../edit/edit.dart';
import '../news/news.dart';
import '../lab/lab.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  late TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = new TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red,
        leading: IconButton(
          icon: Icon(Icons.search),
          color: Color(0xff333333),
          onPressed: () {
            print('触摸搜索按钮');
            // 跳转页面的方式
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) => SearchPage(
                  title: 'home页面传递的值',
                ),
              ),
            );
          },
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.edit),
            color: Color(0xff333333),
            onPressed: () {
              print('触摸编辑按钮');
              Navigator.pushNamed(context, '/edit', arguments: 'e2');
            },
          ),
        ],
        title: TabBar(
          controller: tabController,
          labelColor: Colors.greenAccent,
          unselectedLabelColor: Color.fromARGB(255, 185, 46, 46),
          indicatorColor: Colors.greenAccent,
          tabs: [
            Tab(text: 'news'),
            Tab(text: 'lab'),
          ],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          NewsPage(),
          LabPage(),
        ],
      ),
    );
  }
}
