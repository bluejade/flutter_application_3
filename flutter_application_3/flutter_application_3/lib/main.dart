// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import './pages/index/index.dart';
import './pages/edit/edit.dart';

main(List<String> args) {
  return runApp(new MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/app',
      onGenerateRoute: onGenerateRoute,
    );
  }
}

var routes = {
  '/app': (context) {
    return IndexPage();
  },
  '/edit': (context, {arguments}) => EditPage(
        str: arguments,
      )
};

var onGenerateRoute = (RouteSettings setting) {
  print('$setting-----${setting.arguments}');
  // 统一处理传参
  final String? name = setting.name; // '/edit'
  final Function? pageContentBuilder =
      routes[name]; // (BuildContext context) => EditPage()

  if (pageContentBuilder != null) {
    if (setting.arguments != null) {
      final Route route = MaterialPageRoute(
        builder: (context) =>
            pageContentBuilder(context, arguments: setting.arguments), // 调用的地方
      );
      return route;
    } else {
      final Route route = MaterialPageRoute(
        builder: (context) => pageContentBuilder(context),
      );
      return route;
    }
  }
};
