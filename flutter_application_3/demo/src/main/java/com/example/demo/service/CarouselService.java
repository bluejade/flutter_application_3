package com.example.demo.service;

import cn.hutool.json.JSONObject;
import com.example.demo.entity.Carousel;
import com.example.demo.mapper.CarouselMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Service表示我们在业务逻辑层进行操纵，属于自动配置的环节；
 * 相应的五个方法，通过对象得到相应返回值给UserMapper接口。
 */
@Service
public class CarouselService {
    @Autowired
    private CarouselMapper carouselMapper;

    public Carousel getCarousel(int id){
        return carouselMapper.getCarousel(id);
    }

    public int delete(int id){
        return carouselMapper.delete(id);
    }

    public int update(Carousel carousel){
        return carouselMapper.update(carousel);
    }

    public int save(Carousel carousel){
        return carouselMapper.save(carousel);
    }

    public JSONObject selectAll(){
        JSONObject res = new JSONObject();
        res.put("code",200);
        res.put("message","查询成功");
        res.put("data",carouselMapper.selectAll());
        return res;
    }
}
