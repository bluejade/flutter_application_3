package com.example.demo.service;

import cn.hutool.json.JSONObject;
import com.example.demo.entity.Post;
import com.example.demo.mapper.PostMapper;
import com.example.demo.utils.PageRequest;
import com.example.demo.utils.PageResult;
import com.example.demo.utils.PageUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Service表示我们在业务逻辑层进行操纵，属于自动配置的环节；
 * 相应的五个方法，通过对象得到相应返回值给UserMapper接口。
 */
@Service
public class PostService {
    @Autowired
    private PostMapper postMapper;

    public Post getPost(int id){
        return postMapper.getPost(id);
    }

    public int delete(int id){
        return postMapper.delete(id);
    }

    public int update(Post post){
        return postMapper.update(post);
    }

    public int save(Post post){
        return postMapper.save(post);
    }

    public JSONObject selectAll(){
        JSONObject res = new JSONObject();
        res.put("code",200);
        res.put("message","查询成功");
        res.put("data",postMapper.selectAll());
        return res;
    }

    /** 分页查询 */
    /**
     * 调用分页插件完成分页
     * @param pageRequest
     * @return
     */
    public PageResult getAllbyPage(PageRequest pageRequest) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<Post> sysMenus = postMapper.getAllByPage();
        return PageUtils.getPageResult(new PageInfo<Post>(sysMenus));
    }
}
