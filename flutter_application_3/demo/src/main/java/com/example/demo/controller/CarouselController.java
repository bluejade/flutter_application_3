package com.example.demo.controller;

import cn.hutool.json.JSONObject;
import com.example.demo.entity.Carousel;
import com.example.demo.service.CarouselService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @RestController注解：就表示我们在控制层模块。控制层是作为SpringMVC最重要的一个环节，进行前端请求的处理，转发，重定向，还包括调用Service方法；
 * @RequestMapping注解：处理请求和控制器方法之间的映射关系；
 * @ResponseBody注解：将返回的数据结构转换为JSON格式响应到浏览器
 */
@RestController
@RequestMapping("/carousel")
public class CarouselController {
    @Autowired
    private CarouselService carouselService;

    //通过id获取轮播图
    @RequestMapping(value = "/getCarousel/{id}",method = RequestMethod.GET)
    public String getCarousel(@PathVariable int id){
        return carouselService.getCarousel(id).toString();
    }

    // 沟通id删除轮播图
    @RequestMapping(value = "/delete",method = RequestMethod.DELETE)
    public String delete(int id){
        int result=carouselService.delete(id);
        if(result>=1){
            return "删除成功";
        }else {
            return "删除失败";
        }
    }

    // 更新轮播图
    @RequestMapping(value = "/update",method = RequestMethod.PUT)
    public String update(Carousel carousel){
        int result = carouselService.update(carousel);
        if(result>=1){
            return "更新成功";
        }else {
            return "更新失败";
        }
    }

    // 插入轮播图
    @RequestMapping(value = "/insert",method = RequestMethod.POST)
    public int insert(Carousel carousel){
        return carouselService.save(carousel);
    }

    // 查询所有轮播图信息
    @RequestMapping(value = "/selectAll")
    @ResponseBody  //理解为：单独作为响应体，这里不调用实体类的toString方法
    @CrossOrigin(origins = "*")
    public JSONObject carouselList(){

        return carouselService.selectAll();
    }
}
