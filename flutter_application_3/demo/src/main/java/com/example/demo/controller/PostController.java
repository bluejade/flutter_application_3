package com.example.demo.controller;

import cn.hutool.json.JSONObject;
import com.example.demo.entity.Post;
import com.example.demo.service.PostService;
import com.example.demo.utils.PageRequest;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @RestController注解：就表示我们在控制层模块。控制层是作为SpringMVC最重要的一个环节，进行前端请求的处理，转发，重定向，还包括调用Service方法；
 * @RequestMapping注解：处理请求和控制器方法之间的映射关系；
 * @ResponseBody注解：将返回的数据结构转换为JSON格式响应到浏览器
 */
@RestController
@RequestMapping("/post")
public class PostController {
    @Autowired
    private PostService postService;

    //通过id获取轮播图
    @RequestMapping(value = "/getPost/{id}",method = RequestMethod.GET)
    public String getPost(@PathVariable int id){
        return postService.getPost(id).toString();
    }

    // 沟通id删除轮播图
    @RequestMapping(value = "/delete",method = RequestMethod.DELETE)
    public String delete(int id){
        int result=postService.delete(id);
        if(result>=1){
            return "删除成功";
        }else {
            return "删除失败";
        }
    }

    // 更新轮播图
    @RequestMapping(value = "/update",method = RequestMethod.PUT)
    public String update(Post post){
        int result = postService.update(post);
        if(result>=1){
            return "更新成功";
        }else {
            return "更新失败";
        }
    }

    // 插入轮播图
    @RequestMapping(value = "/insert",method = RequestMethod.POST)
    public int insert(Post post){
        return postService.save(post);
    }

    // 查询所有轮播图信息
    @RequestMapping(value = "/selectAll")
    @ResponseBody  //理解为：单独作为响应体，这里不调用实体类的toString方法
    @CrossOrigin(origins = "*")  // 跨域
    public JSONObject postList(){
        return postService.selectAll();
    }

//    // 分页查询
//    // pagehelper 分页 post 请求
//    @RequestMapping(value = "/selectPage")
//    @ResponseBody
//    public Result selectPage(@RequestBody PageRequest pageQuery){
//        return postService.queryUserByParams(post);
//    }

    // pagehelper 分页 get 请求
    @GetMapping(value="/findPagebyGet")
    @CrossOrigin(origins = "*")
    public Object findPagebyGet(PageRequest pageQuery) {
        return postService.getAllbyPage(pageQuery);
    }

    // pagehelper 分页 post 请求
    @PostMapping(value="/findPagebyPost")
    @ResponseBody
    @CrossOrigin(origins = "*")
    public Object findPagebyPost(@RequestBody(required = false) PageRequest pageQuery) {
        return postService.getAllbyPage(pageQuery);
    }
}
