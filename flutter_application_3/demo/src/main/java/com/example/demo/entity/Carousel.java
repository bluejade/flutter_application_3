package com.example.demo.entity;

public class Carousel {
    private Integer id;
    private String name;
    private String carousel;

    public Carousel () {

    }

    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id=id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name=name;
    }

    public String getCarousel(){
        return carousel;
    }

    public void setCarousel(String carousel){
        this.carousel=carousel;
    }

    @Override
    public String toString(){
        return "Carousel{"+
                "id='"+id+'\''+
                ",name="+name+
                ",carousel="+carousel+
                '}';
    }
}
